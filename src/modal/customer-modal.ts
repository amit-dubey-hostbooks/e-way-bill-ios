export interface Customer{
  customer_id?:number,
  customer_name?: string,
	customer_nickname?: string,
  customer_gstin?: string,
	customer_country?: string,
	customer_state?: string,
  customer_contact_person?: string,
  customer_mobile?:number,
  customer_pan?:string,
  customer_address_one?:string,
  customer_address_two?:string,
  customer_city?:string,
  customer_pincode?:number,
  customer_email?:string,
  customer_landline?:number
}
