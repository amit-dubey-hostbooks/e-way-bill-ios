export interface Transport{
  transportion_name?: string,
  gstin?: string,
  approx_distance?: number,
  transportation_mode?: string,
  transportation_date?: string,
  vehicle_type?: string,
  vehicle_number?: string,
  transport_doc_no?: string
}
