import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';

@Injectable()
export class CommonService {

  public selected = null;
  public loading: any;
  public loginData = null;
  public selectedBusiness = null;
  public selectedBusinessGSTINID = null;
  public selectedBillFrom = null;
  public selectedBillTo = null;

  constructor(public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    this.loginData = JSON.parse(localStorage.getItem('loginData'));
    this.selectedBusiness = JSON.parse(localStorage.getItem('selectedBusiness'));
  }

  displayToaster(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  displayLoader(message) {
    this.loading = this.loadingCtrl.create({
      spinner:'ios',
      content: message,
    });
    this.loading.present();
  }

  hideLoader(){
    this.loading.dismiss();
  }

  formatDate(value){
    let date = new Date(value);
    let dd:any;
    let mm:any;
    let yyyy:any = date.getFullYear();

    if(date.getDate()<10){
      dd = `0${date.getDate()}`;
    }else{
      dd = `${date.getDate()}`;
    }
    if(date.getMonth()+1<10){
      mm = `0${date.getMonth()+1}`;
    }
    else{
      mm = `${date.getMonth()+1}`;
    }
    return `${dd}-${mm}-${yyyy}`;
  }

  formatTransportDate(value){
    let date = new Date(value);
    let dd:any;
    let mm:any;
    let yyyy:any = date.getFullYear();

    if(date.getDate()<10){
      dd = `0${date.getDate()}`;
    }else{
      dd = `${date.getDate()}`;
    }
    if(date.getMonth()+1<10){
      mm = `0${date.getMonth()+1}`;
    }
    else{
      mm = `${date.getMonth()+1}`;
    }
    return `${dd}/${mm}/${yyyy}`;
  }

  getFormatedDate(value){
    let date = new Date(value);
    let yyyy:any = date.getFullYear();
    let mm:any;
    let dd:any;

    if(date.getDate()<10){
      dd = `0${date.getMonth()}`;
    }else{
      dd = `${date.getMonth()}`;
    }

    if(date.getMonth()+1==1){
      mm = `Jan`;
    }
    if(date.getMonth()+1==2){
      mm = `Feb`;
    }
    if(date.getMonth()+1==3){
      mm = `Mar`;
    }
    if(date.getMonth()+1==4){
      mm = `Apr`;
    }
    if(date.getMonth()+1==5){
      mm = `May`;
    }
    if(date.getMonth()+1==6){
      mm = `Jun`;
    }
    if(date.getMonth()+1==7){
      mm = `Jul`;
    }
    if(date.getMonth()+1==8){
      mm = `Aug`;
    }
    if(date.getMonth()+1==9){
      mm = `Sep`;
    }
    if(date.getMonth()+1==10){
      mm = `Oct`;
    }
    if(date.getMonth()+1==11){
      mm = `Nov`;
    }
    if(date.getMonth()+1==12){
      mm = `Dec`;
    }

    let returnValue = null;
    if(value!=''){
      returnValue = `${dd} ${mm} ${yyyy}`;
    }
    else{
      returnValue = 'Not Defined';
    }
    return returnValue;

  }

  getSplitedDate(value){

    let date = value.split('/');

    let temp_dd: any;
    let dd = parseInt(date[0]);

    let temp_mm: any;
    let mm = parseInt(date[1]);

    let yyyy = parseInt(date[2]);

    if(mm<10){
      temp_mm = `0${mm}`;
    }
    else{
      temp_mm = `${mm}`;
    }

    if(dd<10){
      temp_dd = `0${dd}`;
    }else{
      temp_dd = `${dd}`;
    }
    return `${temp_dd}-${temp_mm}-${yyyy}`;
  }

  formatReverseDate(value){
    let date = new Date(value);
    let dd:any;
    let mm:any;
    let yyyy:any = date.getFullYear();
    if(date.getMonth()+1<10){
      mm = `0${date.getMonth()+1}`;
    }
    else{
      mm = `${date.getMonth()+1}`;
    }
    if(date.getDate()<10){
      dd = `0${date.getDate()}`;
    }else{
      dd = `${date.getDate()}`;
    }
    return `${yyyy}-${mm}-${dd}`;
  }

  disableDate(value){
    let date = new Date(value);
    let dd:any;
    let mm:any;
    let yyyy:any = date.getFullYear();
    if(date.getMonth()+1<10){
      mm = `0${date.getMonth()+1}`;
    }
    else{
      mm = `${date.getMonth()+1}`;
    }
    if(date.getDate()<10){
      dd = `0${date.getDate()}`;
    }else{
      dd = `${date.getDate()}`;
    }
    return `${yyyy}-${mm}-${dd}`;
  }

  getSplitedFormatedDate(value){

    let date = value.split('/');

    let yyyy:any = parseInt(date[2]);

    let mm:any;
    let dd:any;

    if(parseInt(date[0])<10){
      dd = `0${parseInt(date[0])}`;
    }
    else
    {
      dd = `${parseInt(date[0])}`;
    }

    if(parseInt(date[1])==1){
      mm = `Jan`;
    }
    if(parseInt(date[1])==2){
      mm = `Feb`;
    }
    if(parseInt(date[1])==3){
      mm = `Mar`;
    }
    if(parseInt(date[1])==4){
      mm = `Apr`;
    }
    if(parseInt(date[1])==5){
      mm = `May`;
    }
    if(parseInt(date[1])==6){
      mm = `Jun`;
    }
    if(parseInt(date[1])==7){
      mm = `Jul`;
    }
    if(parseInt(date[1])==8){
      mm = `Aug`;
    }
    if(parseInt(date[1])==9){
      mm = `Sep`;
    }
    if(parseInt(date[1])==10){
      mm = `Oct`;
    }
    if(parseInt(date[1])==11){
      mm = `Nov`;
    }
    if(parseInt(date[1])==12){
      mm = `Dec`;
    }

    let returnValue = null;
    if(value!=''){
      returnValue = `${dd} ${mm} ${yyyy}`;
    }
    else{
      returnValue = 'Not Defined';
    }
    return returnValue;
  }

  emailValidationMessage(code: number){
    let message: any;
    if(code==5){
      message = 'Timeout. Did not get a response in time!';
    }
    else if(code==10){
      message = 'Syntax OK!';
    }
    else if(code==20){
      message = 'Syntax OK and the domain is valid!';
    }
    else if(code==45){
      message = 'Domain is a catch all and does not support validation!';
    }
    else if(code==50){
      message = 'Valid email address!';
    }
    else if(code==100){
      message = 'General syntax error!';
    }
    else if(code==110){
      message = 'Invalid character in address!';
    }
    else if(code==115){
      message = 'Invalid domain syntax!';
    }
    else if(code==120){
      message = 'Invalid username syntax!';
    }
    else if(code==125){
      message = 'Invalid username syntax for that domain!';
    }
    else if(code==130){
      message = 'Address is too long!';
    }
    else if(code==135){
      message = 'Address has unbalanced parentheses!';
    }
    else if(code==140){
      message = 'Address does not have a username!';
    }
    else if(code==145){
      message = 'Address does not have a domain!';
    }
    else if(code==150){
      message = 'Address does not have an @ sign!';
    }
    else if(code==155){
      message = 'Address has more than one @ sign!';
    }
    else if(code==200){
      message = 'Invalid top-level domain (TLD) in address!';
    }
    else if(code==205){
      message = 'IP address is not allowed as a domain!';
    }
    else if(code==210){
      message = 'Address contains an extra space or character!';
    }
    else if(code==215){
      message = 'Unquoted spaces not allowed in email addresses!';
    }
    else if(code==310){
      message = 'Domain does not exist!';
    }
    else if(code==315){
      message = 'Domain does not have a valid IP address!';
    }
    else if(code==325){
      message = 'Domain can not receive email!';
    }
    else if(code==400){
      message = 'Mailbox does not exist!';
    }
    else if(code==410){
      message = 'The mailbox is full and can’t receive email!';
    }
    else if(code==420){
      message = 'Mail is not accepted for this domain!';
    }
    else if(code==500){
      message = 'Emails with that username aren’t accepted!';
    }
    else if(code==505){
      message = 'Emails with that domain aren’t accepted!';
    }
    else if(code==510){
      message = 'That address isn’t accepted!';
    }
    else if(code==520){
      message = 'Address matched to known bouncers (optional feature)!';
    }
    else if(code==525){
      message = 'Address is a spamtrap, a known complainer or is suppressed!';
    }
    else if(code==530){
      message = 'Address has opted out from commercial email!';
    }
    else if(code==999){
      message = 'System error!';
    }
    return message;
  }
}
