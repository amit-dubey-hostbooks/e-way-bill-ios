import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

import { Transaction } from '../../modal/transaction-modal';
import { BillFrom } from '../../modal/bill-from-modal';
import { BillTo } from '../../modal/bill-to-modal';
import { Item } from '../../modal/item-modal';
import { Transport } from '../../modal/transport-modal';

@IonicPage()
@Component({
  selector: 'page-bill-details',
  templateUrl: 'bill-details.html',
})
export class BillDetailsPage {

  billDetails: any;

  transaction: Transaction = {};
  billFrom: BillFrom = {};
  billTo: BillTo = {};
  items: Item = {};
  transport: Transport = {};

  transactionDetails: boolean = false;
  billFromDetails: boolean = true;
  billToDetails: boolean = true;
  itemsDetails: boolean = true;
  transportDetails: boolean = true;

  itemsList: any = [];
  tempItemList: any = [];
  states: any = [];

  fromStateCode: any = 0;
  toStateCode: any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.billDetails = JSON.parse(localStorage.getItem('billDetails'));

    this.fromStateCode = this.billDetails.fromStateCode;
    this.toStateCode = this.billDetails.toStateCode;

    this.itemsList = this.billDetails.itemList;
    console.log(this.billDetails);
  }

  showTransactionDetails(){
    this.transactionDetails = !this.transactionDetails;
  }

  showBillFromDetails(){
    this.billFromDetails = !this.billFromDetails;
  }

  showBillToDetails(){
    this.billToDetails = !this.billToDetails;
  }

  showItemDetails(){
    this.itemsDetails = !this.itemsDetails;
  }

  showTransportDetails(){
    this.transportDetails = !this.transportDetails;
  }

  updateTransaction(){
    let data = {
      header_text: 'Update Transaction',
      mode: 2
    };
    let addTransactionModal = this.modalCtrl.create('AddTransactionPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.transaction = res;
      console.log(this.transaction);
    });
    addTransactionModal.present();
  }

  updateBillFrom(){
    let data = {
      header_text: 'Update Bill From',
      mode: 2
    };
    let addTransactionModal = this.modalCtrl.create('AddBillFromPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.billFrom = res;
      console.log(this.billFrom);
      this.fromStateCode = res.bill_from_state;
    });
    addTransactionModal.present();
  }

  updateBillTo(){
    let data = {
      header_text: 'Update Bill To',
      mode: 2
    };
    let addTransactionModal = this.modalCtrl.create('AddBillToPage', data);
    addTransactionModal.onDidDismiss(res => {
      this.billTo = res;
      console.log(this.billTo);
      this.toStateCode = res.bill_to_state;
    });
    addTransactionModal.present();
  }

  updateItem(item){
    let data = {
      header_text: 'Update Item',
      mode: 2,
      itemData: item
    };
    let addTransactionModal = this.modalCtrl.create('AddItemPage', data);
    addTransactionModal.onDidDismiss(res => {
      (this.itemsList).forEach(element => {
        if(element.item_id==res.item_id){

          element.item_id = res.item_id;
          element.inv_no = res.inv_no;
          element.itemDescription = res.itemDescription;
          element.description = res.itemDescription;
          element.hsnSac = res.hsnSac;
          element.qty = res.qty;
          element.unitofmeasurement = res.unitofmeasurement;

          element.rate = res.rate;
          element.taxablerate = res.taxablerate;
          element.cessrate = res.cessrate;

          let qty = parseFloat(res.qty);
          let tax_rate = parseFloat(res.taxablerate);
          let rate = parseFloat(res.rate);
          let tax_price = rate*qty*tax_rate/100;
          let cess_rate = parseFloat(res.cessrate);

          if(this.fromStateCode==this.toStateCode)
          {
            element.cgst = tax_rate/2;
            element.cgstRate = tax_price/2;

            element.sgst = tax_rate/2;
            element.sgstRate = tax_price/2;

            element.igst = 0;
            element.igstRate = 0;
          }
          else
          {
            element.igst = tax_rate;
            element.igstRate = tax_price;

            element.cgst = 0;
            element.cgstRate = 0;
            element.sgst = 0;
            element.sgstRate = 0;
          }

          element.cessAmt = (rate*qty)*cess_rate/100;
          element.cessAdvol = (rate*qty)*cess_rate/100;
          element.taxablevalue1 =  rate*qty;
          element.total = rate*qty*(100+tax_rate)/100 + (rate*qty)*cess_rate/100;

          console.log(element);
        }
      });
    });
    addTransactionModal.present();
  }

  updateTransport(){
    let data = {
      header_text: 'Update Transport',
      mode: 2
    };
    let addTransportModal = this.modalCtrl.create('AddTransportPage', data);
    addTransportModal.onDidDismiss(res => {
      this.transport = res;
      console.log(this.transport);
    });
    addTransportModal.present();
  }

  addItem(){
    let data = {
      header_text: 'Add Item',
      mode: 1
    };
    let addTransactionModal = this.modalCtrl.create('AddItemPage', data);
    addTransactionModal.onDidDismiss(res => {
      if(res.item_description!=''){

        let data = {
          cessAdvol:null,
          cessAmt:null,
          cessrate:null,
          cgst:null,
          cgstRate:null,
          description:null,
          hsnSac:null,
          igst:null,
          igstRate:null,
          inv_no:null,
          itemDescription:null,
          item_id:null,
          qty:null,
          rate:null,
          sgst:null,
          sgstRate:null,
          taxablerate:null,
          taxablevalue1:null,
          total:null,
          unitofmeasurement:null
        }

        data.item_id = res.item_id;
        data.inv_no = res.inv_no;
        data.itemDescription = res.itemDescription;
        data.description = res.itemDescription;
        data.hsnSac = res.hsnSac;
        data.qty = res.qty;
        data.unitofmeasurement = res.unitofmeasurement;

        data.rate = res.rate;
        data.taxablerate = res.taxablerate;
        data.cessrate = res.cessrate;

        let qty = parseFloat(res.qty);
        let tax_rate = parseFloat(res.taxablerate);
        let rate = parseFloat(res.rate);
        let tax_price = rate*qty*tax_rate/100;
        let cess_rate = parseFloat(res.cessrate);

        if(this.fromStateCode==this.toStateCode)
        {
          data.cgst = tax_rate/2;
          data.cgstRate = tax_price/2;

          data.sgst = tax_rate/2;
          data.sgstRate = tax_price/2;

          data.igst = 0;
          data.igstRate = 0;
        }
        else
        {
          data.igst = tax_rate;
          data.igstRate = tax_price;

          data.cgst = 0;
          data.cgstRate = 0;
          data.sgst = 0;
          data.sgstRate = 0;
        }

        data.cessAmt = (rate*qty)*cess_rate/100;
        data.cessAdvol = (rate*qty)*cess_rate/100;
        data.taxablevalue1 =  rate*qty;
        data.total = rate*qty*(100+tax_rate)/100 + (rate*qty)*cess_rate/100;

        this.itemsList.push(data);
      }
    });
    addTransactionModal.present();
  }

  deleteItem(item){
    if((this.itemsList).length>=2){
      const alert = this.alertCtrl.create({
        title: 'Confirm Delete?',
        message: 'Your item will remove from list.',
        buttons: [
          {
            text: 'No',
            cssClass: 'cancelAlertButton',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            cssClass: 'submitAlertButton',
            handler: data => {
              (this.itemsList).forEach(element => {
                let index = this.itemsList.indexOf(item);
                if (index > -1) {
                  this.itemsList.splice(index, 1);
                }
              });
            }
          }
        ]
      });
      alert.present();
    }
    else{
      this.common.displayToaster('Sorry, You can not delete all item from invoice.');
    }
  }

  save(){
    const alert = this.alertCtrl.create({
      title: 'Save Invoice?',
      message: 'Your changes will be saved.',
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: data => {

            let stateOneCode = (this.billFrom.bill_from_state)?(this.billFrom.bill_from_state):this.billDetails.fromStateCode;
            let stateTowCode = (this.billTo.bill_to_state)?(this.billTo.bill_to_state):this.billDetails.toStateCode;

            let totalCgst = 0;
            let totalSgst = 0;
            let totalIgst = 0;
            let total_cess_value = 0;
            let total_taxable_value = 0;
            let total_invoice_value = 0;

            (this.itemsList).forEach((element)=>{

              let qty = parseFloat(element.qty);
              let tax_rate = parseFloat(element.taxablerate);
              let rate = parseFloat(element.rate);
              let tax_price = (rate*qty)*tax_rate/100;
              let cess_rate = parseFloat(element.cessrate);

              if(stateOneCode==stateTowCode)
              {
                element.cgst = tax_rate/2;
                element.cgstRate = tax_price/2;
                totalCgst = totalCgst + tax_price/2;

                element.sgst = tax_rate/2;
                element.sgstRate = tax_price/2;
                totalSgst = totalSgst + tax_price/2;

                element.igst = 0;
                element.igstRate = 0;
              }
              else
              {
                element.igst = tax_rate;
                element.igstRate = tax_price;
                totalIgst = totalIgst + tax_price;

                element.cgst = 0;
                element.cgstRate = 0;
                element.sgst = 0;
                element.sgstRate = 0;
              }
              total_cess_value = total_cess_value + (rate*qty)*cess_rate/100;
              total_taxable_value = total_taxable_value + rate*qty;
              element.total = rate*qty*(100+tax_rate)/100 + (rate*qty)*cess_rate/100;
              total_invoice_value = total_invoice_value + element.total;
            });

            let fromState = null;
            let toState = null;

            this.states.forEach((element)=>{
              if(element.state_code==stateOneCode){
                fromState = element.state_name;
              }
              if(element.state_code==stateTowCode){
                toState = element.state_name;
              }
            });

            let tempDocDate = this.common.getSplitedDate(this.billDetails.docDate);
            let tempTransDocDate = this.common.getSplitedDate(this.billDetails.transDocDate);

            if(this.transaction.document_date!=undefined){
              tempDocDate = this.common.formatDate(this.transaction.document_date);
            }

            if(this.transport.transportation_date!=undefined){
              tempTransDocDate = this.common.formatDate(this.transport.transportation_date);
            }

            let passingData = {
              userid: this.common.loginData.ID,
              gstinid: this.common.selectedBusiness.GSTIN.gstinid,
              buid: this.common.selectedBusiness.buid,
              inv_no: this.billDetails.inv_no,

              supplyType: (this.transaction.transaction_type)?(this.transaction.transaction_type):this.billDetails.supplyType,
              subSupplyType: (this.transaction.transaction_sub_type)?(this.transaction.transaction_sub_type):this.billDetails.subSupplyType1,
              supplyTypeDesc: null,
              othersubtype: null,
              subSupplyType1: null,
              docType: (this.transaction.document_type)?(this.transaction.document_type):this.billDetails.docType1,
              docType1: null,
              docNo: (this.transaction.document_no)?(this.transaction.document_no):this.billDetails.docNo,
              docDate: tempDocDate,

              fromGstin: (this.billFrom.bill_from_gstin)?(this.billFrom.bill_from_gstin):this.billDetails.fromGstin,
              fromSupply: (this.billFrom.bill_from_state_code)?(this.billFrom.bill_from_state_code):this.billDetails.fromSupply,
              fromSupplyDesc: null,
              fromTrdName: (this.billFrom.bill_from_name)?(this.billFrom.bill_from_name):this.billDetails.fromTrdName,
              fromAddr1: (this.billFrom.bill_from_address_one)?(this.billFrom.bill_from_address_one):this.billDetails.fromAddr1,
              fromAddr2: (this.billFrom.bill_from_address_two)?(this.billFrom.bill_from_address_two):this.billDetails.fromAddr2,
              fromPlace: (this.billFrom.bill_from_place)?(this.billFrom.bill_from_place):this.billDetails.fromPlace,
              fromPincode: (this.billFrom.bill_from_pincode)?(this.billFrom.bill_from_pincode):this.billDetails.fromPincode,
              fromStateCode: (this.billFrom.bill_from_state)?(this.billFrom.bill_from_state):this.billDetails.fromStateCode,

              toGstin: (this.billTo.bill_to_gstin)?(this.billTo.bill_to_gstin):this.billDetails.toGstin,
              toSupply: (this.billTo.bill_to_state_code)?(this.billTo.bill_to_state_code):this.billDetails.toSupply,
              toSupplyDesc: null,
              toTrdName: ( this.billTo.bill_to_name)?( this.billTo.bill_to_name):this.billDetails.toTrdName,
              toAddr1: (this.billTo.bill_to_address_one)?(this.billTo.bill_to_address_one):this.billDetails.toAddr1,
              toAddr2: (this.billFrom.bill_from_address_two)?(this.billFrom.bill_from_address_two):this.billDetails.toAddr2,
              toPlace: (this.billTo.bill_to_place)?(this.billTo.bill_to_place):this.billDetails.toPlace,
              toPincode: (this.billTo.bill_to_pincode)?(this.billTo.bill_to_pincode):this.billDetails.toPincode,
              toStateCode: (this.billTo.bill_to_state)?(this.billTo.bill_to_state):this.billDetails.toStateCode,

              cgstValue: totalCgst,
              sgstValue: totalSgst,
              igstValue: totalIgst,
              cessValue: total_cess_value,
              totalValue: total_taxable_value,
              totalTaxableValue: total_invoice_value,

              transporterId: null,
              transporterName: (this.transport.transportion_name)?(this.transport.transportion_name):this.billDetails.transporterName,
              transDocNo: (this.transport.transport_doc_no)?(this.transport.transport_doc_no):this.billDetails.transDocNo,
              transMode: (this.transport.transportation_mode)?(this.transport.transportation_mode):this.billDetails.transMode,
              transModeDesc: null,
              transDistance: (this.transport.approx_distance)?(this.transport.approx_distance):this.billDetails.transDistance,
              transDocDate: tempTransDocDate,

              vehicleNo: (this.transport.vehicle_number)?(this.transport.vehicle_number):this.billDetails.vehicleNo,
              vehicleType: (this.transport.vehicle_type)?(this.transport.vehicle_type):this.billDetails.vehicleType,
              isaction: 'U',
              ewbapiNo: null,
              ewbapiDate: null,
              validUpto: null,
              fromStateName: fromState,
              toStateName: toState,
              userGstin: this.common.selectedBusiness.GSTIN.gstin,
              generatedName: null,
              generatedGstin: null,
              isstatus: null,
              rejectStatus: null,

              itemList: this.itemsList,
              vehicleDetaillist: null,
          }

            console.log(passingData);

            this.common.displayLoader('Please wait...');
            this.api.post('add-ewb-bill', passingData).subscribe((res)=>{
              console.log(res);
              if(res['status']==false){
                this.common.displayToaster(res['message']);
              }
              else{
                this.common.displayToaster('Bill Updated Successfully!');
                this.navCtrl.pop();
              }
              this.common.hideLoader();
            },(err)=>{
              console.log(err);
              this.common.displayToaster('Oops, Something went wrong!');
              this.common.hideLoader();
            });
          }
        }
      ]
    });
    alert.present();

  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }
}
