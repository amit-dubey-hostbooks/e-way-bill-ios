import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrintEwayBillPage } from './print-eway-bill';

@NgModule({
  declarations: [
    PrintEwayBillPage,
  ],
  imports: [
    IonicPageModule.forChild(PrintEwayBillPage),
  ],
})
export class PrintEwayBillPageModule {}
