import { Component } from '@angular/core';
import { IonicPage,  NavController, NavParams, AlertController } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

import { TranslateService } from './../../providers/translate';
import { ApiService } from './../../providers/api';
import { CommonService } from './../../providers/common';

@IonicPage()
@Component({
  selector: 'page-scan-item',
  templateUrl: 'scan-item.html',
})

export class ScanItemPage {

  scanSub: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public iab: InAppBrowser, public qrScanner: QRScanner, public alertCtrl: AlertController, public api: ApiService, public common: CommonService) {
  }

  ionViewDidEnter(){
    this.scanItem();
  }

  scanItem(){
    this.showCamera();
    this.qrScanner.prepare()
    .then((status: QRScannerStatus) => {
      if(status.authorized) {
        this.qrScanner.show().then((data : QRScannerStatus)=> {
          console.log('Data Showing: ', data.showing);
          this.startScan();
        }).catch((err) => {
          this.common.displayToaster(`Error is ${err}`);
        });
      }
      else if(status.denied)
      {
        console.log('Camera permission denied');
        this.common.displayToaster('Camera permission denied');
      }
      else
      {
        console.log('Permission denied for this runtime.');
        this.common.displayToaster('Permission denied for this runtime.');
      }
    })
    .catch((err: any) => {
      console.log('Error is', err);
      this.common.displayToaster(`Error is ${err}`);
    });
  }

  startScan(){
    this.scanSub = this.qrScanner.scan().subscribe((value: any) => {
      console.log('Scanned Data', value);
      let ocrloader = document.getElementById('ocrloader');
      ocrloader.style.display = 'none';
      this.qrScanner.hide();
      this.scanSub.unsubscribe();
      this.hideCamera();

      const alert = this.alertCtrl.create({
        title: 'Scanned Data',
        message: value,
        buttons: [
          {
            text: 'Back',
            cssClass:'cancelAlertButton',
            handler: data => {
              this.navCtrl.pop();
            }
          },
          {
            text: 'Go',
            cssClass:'submitAlertButton',
            handler: data => {
              const browser = this.iab.create(value, '_self', 'location=yes');
              browser.show();
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
    });
  }

  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  }

  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  }

  stopScan(){
    this.qrScanner.hide();
    this.scanSub.unsubscribe();
    this.hideCamera();
    this.navCtrl.pop();
  }
}
