import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Vehicle } from '../../modal/vehicle-modal';

@IonicPage()
@Component({
  selector: 'page-vehicle-update',
  templateUrl: 'vehicle-update.html',
})
export class VehicleUpdatePage {

  header_text: string = '';
  vehicle: Vehicle = {};
  maxDate: any;
  states: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public common: CommonService, public api: ApiService) {
    this.header_text = this.navParams.get('header_text');
    let date = new Date();
    this.maxDate = this.common.disableDate(date);
    this.getStates();
  }

  update(){
    let data = this.vehicle;
    this.viewCtrl.dismiss(data);
  }

  cancel(){
    let data = { };
    this.viewCtrl.dismiss(data);
  }

  getStates(){
    this.api.get('getstate').subscribe((res)=>{
      this.states = JSON.parse(atob(res['stateList']));
      console.log(this.states);
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

}
