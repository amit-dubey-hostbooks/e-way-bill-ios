import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBillFromPage } from './add-bill-from';

@NgModule({
  declarations: [
    AddBillFromPage,
  ],
  imports: [
    IonicPageModule.forChild(AddBillFromPage),
  ],
})
export class AddBillFromPageModule {}
