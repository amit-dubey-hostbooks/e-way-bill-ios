import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { Transaction } from '../../modal/transaction-modal';

@IonicPage()
@Component({
  selector: 'page-add-transaction',
  templateUrl: 'add-transaction.html',
})
export class AddTransactionPage {

  transaction: Transaction = {};
  header_text: string = '';
  maxDate: any;
  mode: number = 1;
  transactionSubTypeList: any;
  documentTypeList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public translate: TranslateService, public common: CommonService) {
    let date = new Date();
    this.maxDate = this.common.disableDate(date);

    this.header_text = this.navParams.get('header_text');
    this.mode = this.navParams.get('mode');
    if(this.mode==2){
      let data = JSON.parse(localStorage.getItem('billDetails'));
      this.transaction.transaction_type = data.supplyType;
      this.transaction.transaction_sub_type = data.subSupplyType1;
      this.transaction.document_type = data.docType1;
      this.transaction.document_no = data.docNo;
      this.transaction.document_date = this.common.formatReverseDate(data.docDate);
    }
  }

  ionViewDidEnter(){
    this.initializeType();
  }

  save(){
    let data = this.transaction;
    this.viewCtrl.dismiss(data);
  }

  cancel(){
    let data = { };
    this.viewCtrl.dismiss(data);
  }

  changeType(){
    let type = this.transaction.transaction_type;
    if(type=='O'){
      this.transactionSubTypeList = [
        {value:12, name: 'Exhibition or Fairs'},
        {value:3, name: 'Export'},
        {value:5, name: 'For Own Use'},
        {value:4, name: 'Job Work'},
        {value:10, name: 'Line Sales'},
        {value:8, name: 'Others'},
        {value:11, name: 'Recepient Not Known'},
        {value:9, name: 'SKD/CKD'},
        {value:1, name: 'Supply'}
      ];

      this.documentTypeList = [
        {value:'BOE', name: 'Advance Receipt'},
        {value:'BIL', name: 'Bills of Supply'},
        {value:'CNT', name: 'Credit Note'},
        {value:'CHL', name: 'Delivery Challan'},
        {value:'OTH', name: 'Other'},
        {value:'INV', name: 'Sales Invoice'}
      ];
    }
    else
    {
      this.transactionSubTypeList = [
        {value:12, name: 'Exhibition or Fairs'},
        {value:2, name: 'Import'},
        {value:5, name: 'For Own Use'},
        {value:6, name: 'Job work Returns'},
        {value:8, name: 'Others'},
        {value:7, name: 'Sales Return'},
        {value:9, name: 'SKD/CKD'},
        {value:1, name: 'Supply'}
      ];

      this.documentTypeList = [
        {value:'BOE', name: 'Advance Receipt'},
        {value:'BIL', name: 'Bills of Supply'},
        {value:'CNT', name: 'Credit Note'},
        {value:'CHL', name: 'Delivery Challan'},
        {value:'OTH', name: 'Other'},
        {value:'INV', name: 'Purchase Invoice'}
      ];
    }
  }

  initializeType(){

    this.transactionSubTypeList = [
      {value:12, name: 'Exhibition or Fairs'},
      {value:3, name: 'Export'},
      {value:5, name: 'For Own Use'},
      {value:4, name: 'Job Work'},
      {value:10, name: 'Line Sales'},
      {value:8, name: 'Others'},
      {value:11, name: 'Recepient Not Known'},
      {value:9, name: 'SKD/CKD'},
      {value:1, name: 'Supply'}
    ];

    this.documentTypeList = [
      {value:'BOE', name: 'Advance Receipt'},
      {value:'BIL', name: 'Bills of Supply'},
      {value:'CNT', name: 'Credit Note'},
      {value:'CHL', name: 'Delivery Challan'},
      {value:'OTH', name: 'Other'},
      {value:'INV', name: 'Sales Invoice'}
    ];

  }
}
