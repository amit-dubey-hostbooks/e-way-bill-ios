import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';

@IonicPage()
@Component({
  selector: 'page-language',
  templateUrl: 'language.html',
})

export class LanguagePage {

  language: any = { name: 'English', translated:'English', code: 'en' };

  constructor(public translate: TranslateService, public common: CommonService, public navCtrl: NavController, public navParams: NavParams) {
  }

  selectLanguage(value){
    this.language = value;
  }

  continue(){
    localStorage.setItem('language', this.language.code);
    this.translate.selectedlanguage = this.language.code;

    let user = localStorage.getItem('loginData');
    let language = localStorage.getItem('language');

    if(user!=null && language!=null){
      this.navCtrl.setRoot('HomePage');
    }
    else{
      this.navCtrl.setRoot('LoginPage');
    }

  }

}
