import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  businessList:any = [];
  tempBusinessList: any = [];
  businessStatus: any;
  displayAddBusinessButton: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
  }

  ionViewDidEnter(){
    this.getBusinessList();
  }

  getSortedName(value){
    let name_array = value.split(' ');
    let firstChar: any = (name_array[0]).charAt(0);
    let secondChar: any;
    if(name_array[1]==undefined || name_array[1]==null){
      secondChar = (name_array[0]).charAt(1);
    }
    else{
      secondChar = (name_array[1]).charAt(0);
    }
    return (firstChar+''+secondChar);
  }

  getSpliceName(value){
    let returnName = null;
    if(value.length>35){
      returnName = `${value.substring(0,35)}...`;
    }
    else{
      returnName = value;
    }
    return returnName;
  }

  addBusiness(){
    let data = {
      isBusiness: true,
      title_text: 'Add New Business',
      business_name: null,
      userid: this.common.loginData.ID,
      gstinid: 0,
      buid: 0,
      mode: 0,
      action: 'B'
    };
    console.log(data);
    let addBusinessModal = this.modalCtrl.create('AddBusinessPage', data);
    addBusinessModal.onDidDismiss(data => {
      console.log(data);
      if(data.role!='cancel'){
        this.getBusinessList();
      }
      else{
        this.common.displayToaster('Add Business Cancelled!');
      }
    });
    addBusinessModal.present();
  }

  addBranch(business_name, buid){
    let data = {
      isBusiness: false,
      title_text: 'Add New Branch',
      business_name: business_name,
      userid: this.common.loginData.ID,
      gstinid: 0,
      buid: buid,
      mode: 1,
      action: 'G'
    };
    console.log(data);
    let addBusinessModal = this.modalCtrl.create('AddBusinessPage', data);
    addBusinessModal.onDidDismiss(data => {
      console.log(data);
      if(data.role!='cancel'){
        this.getBusinessList();
      }
      else{
        this.common.displayToaster('Add Branch Cancelled!');
      }
    });
    addBusinessModal.present();
  }

  gotoDashboard(name, id, branch){

    let data = {
      business_name: name,
      buid: id,
      GSTIN: branch
    }
    this.common.selectedBusiness = data;

    localStorage.setItem('selectedBusiness', JSON.stringify(data));

    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(data.buid);
    let gstinid = btoa(data.GSTIN.gstinid);

    this.common.displayLoader('Please wait...');
    this.api.get(`getitemmster/${userid}/${buid}/${gstinid}/0/L/`).subscribe((res)=>{
      console.log(res);
      if(res['status']==true){
        localStorage.setItem('itemList', atob(res['itemList']));
      }
      else{
        localStorage.setItem('itemList',  JSON.stringify([]));
      }
      this.api.get(`getclient/${userid}/${buid}/${gstinid}/0/L/`).subscribe((res)=>{
        console.log(res);
        if(res['status']==true){
          localStorage.setItem('clientList', atob(res['client']));
        }
        else{
          localStorage.setItem('clientList', JSON.stringify([]));
        }
        this.common.hideLoader();
        this.navCtrl.push('SidemenuPage');
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
        this.common.hideLoader();
      });
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

  showDetails(value){
    this.businessStatus = value;
    this.displayAddBusinessButton = !this.displayAddBusinessButton;
  }

  hideDetails(value){
    this.businessStatus = 0;
    this.displayAddBusinessButton = !this.displayAddBusinessButton;
  }

  getBusinessList(){
    let id = btoa(this.common.loginData.ID);
    this.common.displayLoader('Please wait...');
    this.api.get(`get-business-list/${id}`).subscribe((res)=>{
      console.log(res);
      if(res['status']==false){
        this.common.displayToaster(res['message']);
      }
      else{
        console.log(JSON.parse(atob(res['businessList'])));
        localStorage.setItem('businessList', atob(res['businessList']));
        this.businessList = (JSON.parse(atob(res['businessList']))).reverse();
        this.tempBusinessList = (JSON.parse(atob(res['businessList']))).reverse();
      }
      this.common.hideLoader();
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

  searchBusiness(event){
    this.initializeBusiness();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.businessList = this.businessList.filter((business) => {
        return (((business.business_name).toLowerCase()).indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  initializeBusiness(){
    this.businessList = this.tempBusinessList;
  }

  gotoProfile(){
    let data = {
      header_text: 'My Profile',
    }
    let addHomeProfileModal = this.modalCtrl.create('HomeProfilePage', data);
    addHomeProfileModal.onDidDismiss(data => {
      console.log(data);
    });
    addHomeProfileModal.present();
  }

  logout(){
    const prompt = this.alertCtrl.create({
      title: 'Confirm',
      message: "Are you sure you want to logout?",
      buttons: [
        {
          text: 'No',
          cssClass: 'cancelAlertButton',
          handler: data => {
            console.log('QR & BarCode Generated!');
          }
        },
        {
          text: 'Yes',
          cssClass: 'submitAlertButton',
          handler: data => {
            localStorage.clear();
            this.common.displayToaster('Logged Out Successfully!');
            localStorage.setItem('language', this.translate.selectedlanguage);
            this.navCtrl.setRoot('LoginPage');
          }
        }
      ]
    });
    prompt.present();
  }
}
