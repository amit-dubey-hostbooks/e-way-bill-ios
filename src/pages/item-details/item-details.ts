import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Item } from '../../modal/item-modal';

@IonicPage()
@Component({
  selector: 'page-item-details',
  templateUrl: 'item-details.html',
})
export class ItemDetailsPage {

  item: Item = {};
  button_text: string;
  itemData: any = {};
  cessAmount:number = 0;

  hsnCodeList: any = [];
  temphsnCodeList: any = [];

  displayHsnBar: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,  public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.button_text = this.navParams.get('button_text');
    this.itemData = this.navParams.get('itemData');
    this.item.item_type = 'Goods';

    if(this.itemData != null){
      this.item.item_id = parseInt(this.itemData.itemid);
      this.item.item_description = (this.itemData.item_description)?this.itemData.item_description:'';
      this.item.item_type = (this.itemData.item_type)?this.itemData.item_type:'Goods';
      this.item.item_hsn_sac_code = (parseInt(this.itemData.hsn_sac_code))?parseInt(this.itemData.hsn_sac_code):0;
      this.item.item_sku_code = (this.itemData.item_sku_code)?this.itemData.item_sku_code:'';
      this.item.item_tax_rate = (parseFloat(this.itemData.tax_rate))?parseFloat(this.itemData.tax_rate):0;
      this.item.item_cess_rate = (parseFloat(this.itemData.cess_rate))?parseFloat(this.itemData.cess_rate):0;
      this.item.item_cess_amount = (parseFloat(this.itemData.cess_amount))?parseFloat(this.itemData.cess_amount):0;
      this.item.item_purchase_price = (parseFloat(this.itemData.purchase_price))?parseFloat(this.itemData.purchase_price):0;
      this.item.item_selling_price = (parseFloat(this.itemData.selling_price))?parseFloat(this.itemData.selling_price):0;
      this.item.item_unit = (this.itemData.unit)?this.itemData.unit:'';
      this.item.item_note = (this.itemData.item_notes)?this.itemData.item_notes:'';
    }
    console.log(this.itemData);
  }

  changePrice(){
    this.cessAmount = this.item.item_selling_price*this.item.item_cess_rate/100;
    this.item.item_cess_amount = this.cessAmount;
  }

  save(){
    let itemid: any = 0;
    let action: any = 'I';
    let message = 'Item added successfully!';

    if(this.item.item_id){
      itemid = this.item.item_id;
      action = 'U';
      message = 'Item updated successfully!';
    }

    let itemNotes = '';

    if(this.item.item_note==undefined || this.item.item_note==null || this.item.item_note=='')
    {
      itemNotes = 'New';
    }
    else
    {
      itemNotes = this.item.item_note;
    }

    let data = {
      userid: this.common.loginData.ID,
      itemid: itemid,
      gstinid: this.common.selectedBusiness.GSTIN.gstinid,
      buid: this.common.selectedBusiness.buid,
      item_description: this.item.item_description,
      item_type: this.item.item_type,
      hsn_sac_code: this.item.item_hsn_sac_code,
      item_sku_code: this.item.item_sku_code,
      tax_rate: this.item.item_tax_rate,
      cess_rate: (this.item.item_cess_rate>0)?(this.item.item_cess_rate):0,
      cess_amount: (this.cessAmount>0)?(this.cessAmount):0,
      purchase_price: this.item.item_purchase_price,
      selling_price: this.item.item_selling_price,
      unit: this.item.item_unit,
      discount: 0,
      item_notes: itemNotes,
      inclusive_tax: 0,
      action: action
    };
    console.log(data);
    this.common.displayLoader('Please wait...');
    this.api.post('additemmster', data).subscribe((res)=>{
      this.common.hideLoader();
      if((res['status']==false)&&(parseInt(res['message'])==1))
      {
        this.common.displayToaster('Sorry, Item already present in your list!');
      }
      else if(res['status']==false)
      {
        this.common.displayToaster(res['message']);
      }
      else
      {
        this.common.displayToaster(message);
        this.viewCtrl.dismiss(res);
      }
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
      this.common.hideLoader();
    });
  }

  cancel(){
    let data = { role: 'cancel' };
    this.viewCtrl.dismiss(data);
  }

  searchHsnCodes(event){
    this.displayHsnBar = true;
    let id = event.target.value;
    if((id.toString()).length>=1){
      this.api.get(`gethsn/${id}`).subscribe((res)=>{
        if((res['status']==true)&&((JSON.parse(atob(res['hsn'])).length>=1))){
          this.hsnCodeList = JSON.parse(atob(res['hsn']));
          this.temphsnCodeList = JSON.parse(atob(res['hsn']));
        }
        else{
          this.hsnCodeList = [{hsncodes: 'No Record Found!'}];
        }
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
      });
    }
  }

  selectedCode(value){
    this.displayHsnBar = false;
    console.log(value);
    this.item.item_hsn_sac_code = value.hsncodes;
    if(!isNaN(value.cess)&&(parseFloat(value.cess)>0)){
      this.item.item_cess_rate = parseFloat(value.cess);
    }
    else
    {
      this.item.item_cess_rate = 0;
    }
    if(!isNaN(value.rate)&&(parseFloat(value.rate)>0)){
      this.item.item_tax_rate = parseFloat(value.rate);

    }else{
      this.item.item_tax_rate = 0;
    }
    this.changePrice();
  }

}
