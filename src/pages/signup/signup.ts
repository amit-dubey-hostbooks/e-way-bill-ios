import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { User } from '../../modal/user-modal';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  user: User = {};
  invalidName: boolean = false;
  invalidMobile: boolean = false;
  invalidEmail: boolean = false;
  invalidEmailMessage: string = '';
  invalidPassword: boolean = false;
  invalidPasswordLength: boolean = false;
  invalidConfirmPassword: boolean = false;
  invalidMatch: boolean = false;
  invalidUserType: boolean = false;
  displayPassword: boolean = true;
  hidePassword: boolean = false;

  constructor(public translate: TranslateService, public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController, public api: ApiService, public common: CommonService) {
  }

  validateEmail(){
    this.api.validateEmail(this.user.email).subscribe((res)=>{
      let data = res['email_validation'];
      let code = data.status_code;
      let message = this.common.emailValidationMessage(code);
      if(code==50){
        this.invalidEmailMessage = message;
        this.invalidEmail = false;
      }
      else{
        this.invalidEmail = true;
        this.invalidEmailMessage = message;
      }
    },(err)=>{
      console.log(err);
    });
  }

  register(value: any){
    const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z]+\.[a-zA-Z]{2,4}$/;
    if(this.user.name==undefined || this.user.name==null || this.user.name==''){
      this.invalidName = true;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = false;
      return false;
    }
    else if(isNaN(this.user.mobile) || ((((this.user.mobile).toString())).length!=10)){
      this.invalidName = false;
      this.invalidMobile = true;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = false;
      return false;
    }
    else if ((this.user.email==undefined  || this.user.email=='' || this.user.email.length<=5 || !emailRegexp.test(this.user.email))) {
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = true;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = false;
      return false;
    }
    else if(this.user.password==undefined || this.user.password==null || this.user.password=='') {
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = true;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = false;
      return false;
    }
    else if((this.user.password).length<6) {
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = true;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = false;
      return false;
    }
    else if(this.user.confirm_password==undefined || this.user.confirm_password==null || this.user.confirm_password==''){
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = true;
      this.invalidMatch = false;
      this.invalidUserType = false;
      return false;
    }
    if(this.user.password!=this.user.confirm_password){
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = true;
      this.invalidUserType = false;
      return false;
    }
    else if(this.user.who==undefined || this.user.who==null || this.user.who==''){
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = true;
      return false;
    }
    else{
      this.invalidName = false;
      this.invalidMobile = false;
      this.invalidEmail = false;
      this.invalidPassword = false;
      this.invalidPasswordLength = false;
      this.invalidConfirmPassword = false;
      this.invalidMatch = false;
      this.invalidUserType = false;

      let data = {
        id: 0,
        name: this.user.name,
        email: (this.user.email).toLowerCase(),
        phoneno: this.user.mobile,
        password: this.user.password,
        cnfPassword: this.user.confirm_password,
        userType: this.user.who,
        designation: 'Client'
      };
      console.log(data);
      this.common.displayLoader('Please wait...');
      this.api.post('signup', data).subscribe((res)=>{
        if(res['status']==false)
        {
          this.common.displayToaster(res['message']);
          this.common.hideLoader();
        }
        else
        {
          this.common.displayToaster('Success!');
          this.common.hideLoader();
          const prompt = this.alertCtrl.create({
            title: 'Success!',
            message: `A link has been sent to your email address <b>${data.email}</b>. Visit that link to complete registration. Do check your spam folder, If you do not find it in your inbox.`,
            buttons: [
              {
                text: 'OK',
                cssClass: 'submitAlertButton',
                handler: data => {
                  console.log(res);
                  this.navCtrl.pop();
                }
              }
            ]
          });
          prompt.present();
        }
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
        this.common.hideLoader();
      });
    }
  }

  login(){
    this.navCtrl.pop();
  }

  viewPassword(){
    this.displayPassword = !(this.displayPassword);
    this.hidePassword = !(this.hidePassword);
  }

  generatePassword(){
    let password = Math.random().toString(36).substr(2, 8);
    this.user.password = password;
    this.user.confirm_password = password;
  }

}
