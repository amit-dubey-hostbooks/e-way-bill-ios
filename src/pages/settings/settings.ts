import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  email: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.email = this.common.loginData['Email'];
  }

  updatePassword(){
    const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z]+\.[a-zA-Z]{2,4}$/;
    if (this.email == '') {
      this.common.displayToaster('Please enter your email address');
      return false;
    }
    if ((this.email.length <= 5 || !emailRegexp.test(this.email))) {
      this.common.displayToaster('Please enter an valid email address');
    }
    else{
      let data = {
        forgotemail: (this.email).toLowerCase(),
        username: (this.email).toLowerCase(),
      };
      console.log(data);

      this.common.displayLoader('Please wait...');
      this.api.post('forgot-password', data).subscribe((res)=>{
        this.common.hideLoader();
        if(res['status']==false)
        {
          this.common.displayToaster(res['message']);
        }
        else
        {
          this.common.displayToaster('Success!');
          const prompt = this.alertCtrl.create({
            title: 'Reset Password!',
            message: `A link has been sent to your email address <b>${this.email}</b>. Visit that link to reset your password. Do check your spam folder if you do not find in your spam.`,
            buttons: [
              {
                text: 'Okay!',
                cssClass: 'submitAlertButton',
                handler: data => {
                  console.log(res);
                  this.navCtrl.pop();
                }
              }
            ]
          });
          prompt.present();
        }
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
        this.common.hideLoader();
      });
    }
  }
}
