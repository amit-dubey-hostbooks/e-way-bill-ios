import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController } from 'ionic-angular';
import { TranslateService } from './../../providers/translate';
import { CommonService } from './../../providers/common';
import { ApiService } from './../../providers/api';
import { Item } from '../../modal/item-modal';


@IonicPage()
@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
})
export class AddItemPage {

  item: Item = {};
  header_text: string = '';

  hsnCodeList: any = [];
  temphsnCodeList: any = [];

  itemsList: any = [];
  tempItemsList: any = [];

  displayItems: boolean = false;
  displayHsnBar: boolean = false;

  taxableValue: number = 0;
  cessAmount: number = 0;
  total: number = 0;

  mode: number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public actionSheetCtrl: ActionSheetController, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.header_text = this.navParams.get('header_text');
    this.itemsList = JSON.parse(localStorage.getItem('itemList'));
    this.tempItemsList = JSON.parse(localStorage.getItem('itemList'));
    this.item.item_id = 0;
    this.item.item_type = 'Goods';

    this.mode = this.navParams.get('mode');
    if(this.mode==2){
      let data = this.navParams.get('itemData');
      this.item.item_id = (parseInt(data['item_id']))?parseInt(data['item_id']):0;
      this.item.item_description = (data['itemDescription'])?data['itemDescription']:'';
      this.item.item_quantity = (parseInt(data['qty']))?parseInt(data['qty']):0;
      this.item.item_type = (data['item_type'])?data['item_type']:'Goods';
      this.item.item_hsn_sac_code = (parseInt(data['hsnSac']))?parseInt(data['hsnSac']):null;
      this.item.item_sku_code = (data['hsnSac'])?data['hsnSac']:'';
      this.item.item_tax_rate = (parseFloat(data['taxablerate']))?parseFloat(data['taxablerate']):0;
      this.item.item_cess_rate = (parseFloat(data['cessrate']))?parseFloat(data['cessrate']):0;
      this.item.item_cess_amount = (parseFloat(data['cessAmt']))?parseFloat(data['cessAmt']):0;
      this.item.item_purchase_price = (parseFloat(data['purchase_price']))?parseFloat(data['purchase_price']):0;
      this.item.item_selling_price = (parseFloat(data['rate']))?parseFloat(data['rate']):0;
      this.item.item_unit = (data['unitofmeasurement'])? data['unitofmeasurement']:'';
      this.item.item_note = (data['item_notes'])?data['item_notes']:'';
    }
  }

  cancel(){
    let data = { };
    this.viewCtrl.dismiss(data);
  }

  changePrice(){
    this.taxableValue = this.item.item_selling_price*this.item.item_quantity;
    this.cessAmount = (this.taxableValue*this.item.item_cess_rate/100);
    this.total = (this.taxableValue*(100+this.item.item_tax_rate)/100)+this.cessAmount;
    this.item.item_cess_amount = this.cessAmount;
  }

  save(){
    let data = {
      item_id: this.item.item_id,
      inv_no: null,
      itemDescription: this.item.item_description,
      description: this.item.item_description,
      hsnSac: this.item.item_hsn_sac_code,
      qty: this.item.item_quantity,
      unitofmeasurement: this.item.item_unit,
      rate: this.item.item_selling_price,
      taxablevalue1: this.taxableValue,
      taxablerate: this.item.item_tax_rate,
      cessrate: this.item.item_cess_rate,
      cgst: 0,
      cgstRate: 0,
      sgst: 0,
      sgstRate: 0,
      igst: 0,
      igstRate: 0,
      cessAmt: this.cessAmount,
      cessAdvol: this.cessAmount,
      total: this.total
    };
    this.viewCtrl.dismiss(data);
  }

  searchItem(event){
    this.displayItems = true;
    this.initializeItems();
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.itemsList = this.itemsList.filter((item) => {
        return (((item.item_description).toLowerCase()).indexOf(val.toLowerCase()) > -1);
      });
    }
    if((this.itemsList).length==0){
      (this.itemsList).push({item_description: 'No Record Found!', hsn_sac_code: ''});
    }
  }

  initializeItems(){
    this.itemsList = this.tempItemsList;
  }

  selectedItem(value){
    this.displayItems = false;
    let userid = btoa(this.common.loginData.ID);
    let buid = btoa(this.common.selectedBusiness.buid);
    let gstinid = btoa(this.common.selectedBusiness.GSTIN.gstinid);
    let itemid = parseInt(value.itemid);
    let isaction = 'V';
    this.api.get(`getitemmster/${userid}/${buid}/${gstinid}/${itemid}/${isaction}`).subscribe((res)=>{
      if(res['status']==true){
        let data = JSON.parse(atob(res['itemList']))[0];
        this.item.item_id = (parseInt(data['itemid']))?parseInt(data['itemid']):0;
        this.item.item_description = (data['item_description'])?data['item_description']:'';
        this.item.item_type = (data['item_type'])?data['item_type']:'Goods';
        this.item.item_hsn_sac_code = (parseInt(data['hsn_sac_code']))?parseInt(data['hsn_sac_code']):null;
        this.item.item_sku_code = (data['item_sku_code'])?data['item_sku_code']:'';
        this.item.item_tax_rate = (parseFloat(data['tax_rate']))?parseFloat(data['tax_rate']):0;
        this.item.item_cess_rate = (parseFloat(data['cess_rate']))?parseFloat(data['cess_rate']):0;
        this.item.item_cess_amount = (parseFloat(data['cess_amount']))?parseFloat(data['cess_amount']):0;
        this.item.item_purchase_price = (parseFloat(data['purchase_price']))?parseFloat(data['purchase_price']):0;
        this.item.item_selling_price = (parseFloat(data['selling_price']))?parseFloat(data['selling_price']):0;
        this.item.item_unit = (data['unit'])? data['unit']:'';
        this.item.item_note = (data['item_notes'])?data['item_notes']:'';
      }
      console.log(JSON.parse(atob(res['itemList']))[0]);
    }, (err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Something went wrong!');
    });
  }

  searchHsnCodes(event){
    this.displayHsnBar = true;
    let id = event.target.value;
    if((id.toString()).length>=1){
      this.api.get(`gethsn/${id}`).subscribe((res)=>{
        if((res['status']==true)&&((JSON.parse(atob(res['hsn'])).length>=1))){
          this.hsnCodeList = JSON.parse(atob(res['hsn']));
          this.temphsnCodeList = JSON.parse(atob(res['hsn']));
        }
        else{
          this.hsnCodeList = [{hsncodes: 'No Record Found!'}];
        }
      }, (err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Something went wrong!');
      });
    }
  }

  selectedCode(value){
    this.displayHsnBar = false;
    console.log(value);
    this.item.item_hsn_sac_code = value.hsncodes;
    if(!isNaN(value.cess)&&(parseFloat(value.cess)>0)){
      this.item.item_cess_rate = parseFloat(value.cess);
    }
    else
    {
      this.item.item_cess_rate = 0;
    }
    if(!isNaN(value.rate)&&(parseFloat(value.rate)>0)){
      this.item.item_tax_rate = parseFloat(value.rate);

    }else{
      this.item.item_tax_rate = 0;
    }
    this.changePrice();
  }
}
