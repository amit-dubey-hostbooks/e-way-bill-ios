import { Component, ViewChild, ElementRef  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { TranslateService } from './../../providers/translate';
import { ApiService } from './../../providers/api';
import { CommonService } from './../../providers/common';

declare var google;

@IonicPage()
@Component({
  selector: 'page-driving',
  templateUrl: 'driving.html',
})
export class DrivingPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  map: any;
  options: any;

  origin = { lat: 0, lng: 0 };
  destination: any = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public translate: TranslateService, public api: ApiService, public common: CommonService) {
    this.destination = this.navParams.get('destination');
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {
    this.common.displayLoader('Please wait...');
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 600000, maximumAge: 0 }).then((position) => {
      this.common.hideLoader();
      this.origin.lat = position.coords.latitude;
      this.origin.lng = position.coords.longitude;
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: false
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let myLatLng = {lat: position.coords.latitude, lng: position.coords.longitude};
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: myLatLng
      });
      marker.setMap(this.map);
      this.startNavigating();
    }, (err) => {
      console.log(err);
      this.common.hideLoader();
    });
  }

  startNavigating() {
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(this.map);
    directionsDisplay.setPanel(this.directionsPanel.nativeElement);
    directionsService.route({
      origin: this.origin,
      destination: this.destination || this.origin,
      travelMode: google.maps.TravelMode['DRIVING']
    }, (res, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(res);
      } else {
        console.warn(status);
      }
    });
  }
}
