import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';

import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { SocialSharing } from '@ionic-native/social-sharing';

import { CommonService } from './../../providers/common';

@IonicPage()
@Component({
  selector: 'page-share-file',
  templateUrl: 'share-file.html',
})
export class ShareFilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public filePath: FilePath, public fileChooser: FileChooser, public socialSharing: SocialSharing, public common: CommonService) {
  }

  openFileChooser(){
    this.fileChooser.open().then((url) => {
      console.log(url);
      this.openFilePath(url);
    }).catch((e) => {
      console.log(e);
      this.common.displayToaster('Oops, Unable to process file opener!');
    });
  }

  openFilePath(url){
    this.filePath.resolveNativePath(url).then((path) => {
      console.log(path);
      this.selectOpener(path);
    }).catch((err) => {
      console.log(err);
      this.common.displayToaster('Oops, Unable to process file path!');
    });
  }

  selectOpener(path){
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Share With',
      buttons: [
        {
          text: 'WhatsApp',
          handler: () => {
            this.shareOnWhatsApp(path);
          }
        },
        {
          text: 'Email',
          handler: () => {
            this.shareOnEmail(path);
          }
        },
        {
          text: 'Facebook',
          handler: () => {
            this.shareOnFacebook(path);
          }
        },
        {
          text: 'Twitter',
          handler: () => {
            this.shareOnTwitter(path);
          }
        }
      ]
    });
    actionSheet.present();
  }

  shareOnWhatsApp(path){
    let message = `Dear Customer, Please download HB E-Way Bill mobile application for better convenience & ease. Please visit https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill  or  `;
    let file = path;
    let url = 'https://www.hostbooks.com/in/eway-bill/';
    this.socialSharing.shareViaWhatsApp(message, file, url).then((res)=>{
      console.log(res);
      this.common.displayToaster('Your file has bees shared successfully!');
    }).catch((err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Unable to share file on WhatsApp!');
    });
  }

  shareOnEmail(path){
    let message = `Dear Customer, Please download HB E-Way Bill mobile application for better convenience & ease. Please visit https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill  or  https://www.hostbooks.com/in/eway-bill/`;
    let subject = 'E-Way Bill Invoice';
    let file = path;
    this.socialSharing.canShareViaEmail().then(() => {
      this.socialSharing.shareViaEmail(message, subject, null, null, null, file).then((res)=>{
        console.log(res);
        this.common.displayToaster('Your file has bees shared successfully!');
      }).catch((err)=>{
        console.log(err);
        this.common.displayToaster('Oops, Unable to share file on mail!');
      });
    }).catch((err) => {
      console.log(err);
      this.common.displayToaster('Oops, Unable to share file on mail!');
    });

  }

  shareOnFacebook(path){
    let message = `Dear Customer, Please download HB E-Way Bill mobile application for better convenience & ease. Please visit https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill  or  `;
    let file = path;
    let url = 'https://www.hostbooks.com/in/eway-bill/';
    this.socialSharing.shareViaFacebook(message, file, url).then((res)=>{
      console.log(res);
      this.common.displayToaster('Your file has bees shared successfully!');
    }).catch((err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Unable to share file on facebook!');
    });
  }

  shareOnTwitter(path){
    let message = `Dear Customer, Please download HB E-Way Bill mobile application for better convenience & ease. Please visit https://play.google.com/store/apps/details?id=hostbooks.com.eway.bill  or  `;
    let file = path;
    let url = 'https://www.hostbooks.com/in/eway-bill/';
    this.socialSharing.shareViaTwitter(message, file, url).then((res)=>{
      console.log(res);
      this.common.displayToaster('Your file has bees shared successfully!');
    }).catch((err)=>{
      console.log(err);
      this.common.displayToaster('Oops, Unable to share file on twitter!');
    });
  }

  back(){
    this.navCtrl.pop();
  }
}
