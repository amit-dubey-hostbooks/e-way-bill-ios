import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddTransportPage } from './add-transport';

@NgModule({
  declarations: [
    AddTransportPage,
  ],
  imports: [
    IonicPageModule.forChild(AddTransportPage),
  ],
})
export class AddTransportPageModule {}
